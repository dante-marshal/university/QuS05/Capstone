import numpy as np
from math import log2, ceil, floor
from random import randint, normalvariate, gammavariate
from typing import Union, List, Tuple, Dict, Iterator, Callable
from mido import Message, MetaMessage, MidiFile, tempo2bpm as TempoToBpm, MidiTrack as MidoMidiTrack, bpm2tempo as BpmToTempo

class Action:
	RESOLUTION = 1.0 / 16.0
	NEURON_COUNT_TEMPO = 5 # [10, 60], (60, 100], (100, 130], (130, 190], (190, +inf)
	NEURON_COUNT_TS_NUM = 15 # 15 for Numerator (1-16)
	NEURON_COUNT_TS_DENOM = 5 # 5 for Denom : {1, 2, 4, 8, 16}
	NEURON_COUNT_TIME_SIGNATURE = NEURON_COUNT_TS_NUM + NEURON_COUNT_TS_DENOM
	NEURON_COUNT_NOTES = 128 # The 128 notes of MIDI (C4 = 60); Each with 2 states : [Press, Hold]
	PIXELS_COUNT = NEURON_COUNT_TIME_SIGNATURE + NEURON_COUNT_TEMPO + NEURON_COUNT_NOTES
	NEURON_COUNT = NEURON_COUNT_NOTES
	def __init__(self, tempo: int = 120, timeSig: Tuple[int, int] = (4, 4), notes: Dict[int, float] = None):
		self.tempo: int = tempo
		self.timeSig: Tuple[int, int] = timeSig
		self.notes: Dict[int, float] = {} if notes == None else notes
	def __str__(self):
		return 'Action : {0} {1} {2}'.format(self.tempo, self.timeSig, self.notes)
	def __repr__(self):
		return str(self)
	def Copy(self, withNotes: bool = True):
		newNotes = {}
		if withNotes:
			for n in self.notes:
				newNotes[n] = self.notes[n]
		return Action(self.tempo, self.timeSig, newNotes)
	def Serialize(self):
		ret = np.zeros(Action.PIXELS_COUNT)
		if self.tempo <= 60:
			ret[0] = 1.0
		if self.tempo > 60 and self.tempo <= 100:
			ret[1] = 1.0
		if self.tempo > 100 and self.tempo <= 130:
			ret[2] = 1.0
		if self.tempo > 130 and self.tempo <= 190:
			ret[3] = 1.0
		if self.tempo > 190:
			ret[4] = 1.0
		for i in range(1, 16):
			if self.timeSig[0] == i:
				ret[Action.NEURON_COUNT_TEMPO + i] = 1.0
		for i in [1, 2, 4, 8, 16]:
			if self.timeSig[1] == i:
				ret[Action.NEURON_COUNT_TEMPO + Action.NEURON_COUNT_TS_NUM + i] = 1.0
		for i in range(128):
			if i in self.notes:
				v = max(0, min(1, self.notes[i]))
				ret[Action.NEURON_COUNT_TEMPO + Action.NEURON_COUNT_TIME_SIGNATURE + i] = v
		return ret
	@staticmethod
	def Deserialize(data):
		if len(data) < Action.PIXELS_COUNT:
			raise IndexError(F"Invalid Length, Needs to be at least {Action.PIXELS_COUNT}")
		data = np.array(data).reshape(Action.PIXELS_COUNT)
		ret = Action()
		# Tempo
		idx = 0
		ret.tempo = [50, 80, 120, 160, 200][np.argmax(data[idx:Action.NEURON_COUNT_TEMPO])]
		# Time Sig.
		idx = Action.NEURON_COUNT_TEMPO
		ret.timeSig = (
			list(range(1, 16))[np.argmax(data[idx:idx + Action.NEURON_COUNT_TS_NUM])],
			[1, 2, 4, 8, 16][np.argmax(data[idx + Action.NEURON_COUNT_TS_NUM:idx + Action.NEURON_COUNT_TIME_SIGNATURE])],
		)
		# Notes
		idx = Action.NEURON_COUNT_TEMPO + Action.NEURON_COUNT_TIME_SIGNATURE
		for i in range(Action.NEURON_COUNT_NOTES):
			if data[idx + i] > 0:
				ret.notes[i] = data[idx + i]
		return ret
	@staticmethod
	def SerializeAll(data: List) -> np.ndarray:
		dCount = len(data)
		ret = np.zeros((dCount, Action.PIXELS_COUNT), dtype=float)
		for i in range(dCount):
			ret[i, :] = data[i].Serialize()
		return ret
	@staticmethod
	def DeserializeAll(data: np.ndarray) -> List:
		ret = []
		for i in range(data.shape[0]):
			ret.append(Action.Deserialize(data[i, :]))
		return ret
	@staticmethod
	def GenerateRandom():
		rndTempo = ceil(abs(normalvariate(120, 20)))
		rndTimeSig = (
			max(0, min(16, ceil(gammavariate(8, 0.6)))),
			list([1, 2, 4, 8, 16])[max(0, min(4, ceil(gammavariate(10, 0.125))))]
		)
		rndNoteCount = max(0, min(5, floor(gammavariate(16, 0.125))))
		notes = {}
		for i in range(rndNoteCount):
			note = max(0, min(127, floor(normalvariate(66, 8))))
			while note in notes:
				note = max(0, min(127, floor(normalvariate(66, 8))))
			notes[note] = 80.0 / 127.0
		ret = Action(rndTempo, rndTimeSig, notes)
		return ret

class MidiState:
	time: int
	tempo: int
	notesOn: List[Tuple[int, int]]
	notesOff: List[int]
	timeSig: Tuple[int, int]
	def __init__(self, time: int):
		self.time = time
		self.tempo = 120
		self.timeSig = (4, 4)
		self.notesOn = []
		self.notesOff = []
	def Copy(self, time: int = None, withNotesOn: bool = False, withNotesOff: bool = False):
		if time == None:
			time = self.time
		ret = MidiState(time)
		ret.tempo = self.tempo
		ret.timeSig = self.timeSig
		if withNotesOn:
			for n in self.notesOn:
				ret.notesOn.append(n)
		if withNotesOff:
			for n in self.notesOff:
				ret.notesOff.append(n)
		return ret
	def SetTempo(self, v: int) -> None:
		self.tempo = round(v)
	def SetTimeSig(self, numer: int, denom: int):
		self.timeSig = (numer, denom)
	def PressNote(self, note: int, velocity: int):
		# Ignoring Velocity for now ...
		if not note in self.notesOn:
			self.notesOn.append((note, velocity))
			# print("Note Press   @ {:4.1f} : {:3d}#{:3d} {} > {}".format(self.time, note, velocity, self.notesOn, self.notesOff))
	def ReleaseNote(self, note: int):
		# Ignoring Velocity for now ...
		if not note in self.notesOff:
			self.notesOff.append(note)
			# print("Note Release @ {:4.1f} : {:3d}#{:3d} {} > {}".format(self.time, note, velocity, self.notesOn, self.notesOff))
	def __str__(self):
		return '{} : P={}, R={}'.format(
			repr(self),
			self.notesOn, self.notesOff
		)
	def __repr__(self):
		return '{:7.2F} @ {:4d} BPM {}/{}'.format(
			self.time, round(self.tempo), self.timeSig[0], self.timeSig[1]
		)

class MidiTrack:
	name: str
	times: List[int]
	states: Dict[int, MidiState]
	def __init__(self, name: str, ticksPerBeat: int):
		self.name = name
		self.ticksPerBeat = ticksPerBeat
		self.times = []
		self.states = {}
	def Copy(self, newName: str):
		ret = MidiTrack(newName, self.ticksPerBeat)
		for t in self.times:
			ret.times.append(t)
			ret.states[t] = self.states[t].Copy()
		return ret
	@property
	def TimeTotal(self) -> int:
		if len(self.times) > 0:
			return self.times[-1]
		return 0
	# returns : (timeFound, condition) where condition is :
	#	-2 : First Element
	#	-1 : Nearest Element
	#	 0 : Times array is Empty
	#	+1 : Same Element (Exists in times array)
	#	+2 : Last Element
	def FindLatestStateTime(self, time: int) -> Tuple[int, int]:
		idxMax = len(self.times) - 1
		# If Empty List
		if idxMax < 0:
			return (time, 0)
		# If Smaller than First
		if time < self.times[0]:
			return (self.times[0], -2)
		# If Larger than Last
		if time > self.times[idxMax]:
			return (self.times[idxMax], +2)
		# Binary Search
		idxMin = 0
		while idxMin < idxMax:
			idxMid = (idxMax + idxMin) / 2
			midTime = self.times[idxMid]
			if midTime == time:
				return (time, +1)
			elif midTime > time:
				idxMax = idxMid
			elif midTime < time:
				idxMin = idxMid
		return (self.times[min(idxMin, idxMax)], -1)
	def FindLatestStateAt(self, time: int) -> Union[MidiState, None]:
		lastTime = self.FindLatestStateTime(time)
		if lastTime[1] == 0:
			return None
		return self.states[lastTime[0]]
	def GetOrCreateStateAt(self, time: int) -> MidiState:
		# Create a New state if Not found
		if not time in self.states:
			# If there's any state before now, Copy data from last one
			lastState = self.FindLatestStateAt(time)
			if lastState != None:
				newState = lastState.Copy(time)
			else:
				newState = MidiState(time)
			# Set it on Dictionary
			self.states[time] = newState
			self.times.append(time)
			self.times.sort()
		return self.states[time]
	def __str__(self):
		return '{}, State Count : {:4d}, Time Total : {:8.4f}'.format(self.name.rjust(24), len(self.states), self.TimeTotal)
	def Serialize(self):
		ret: List[Action] = []
		lastTick = 0
		ret.append(Action())
		timeDelta = list(self.states.keys())[0]
		for t in self.states:
			# Update Tick
			timeStep = self.ticksPerBeat / ((self.states[t].tempo / 4) / (Action.RESOLUTION * 60))
			tick = round((t - timeDelta) / timeStep)
			# Update Target
			while lastTick < tick:
				oldAction = ret[lastTick]
				newAction = oldAction.Copy(True)
				ret.append(newAction)
				lastTick += 1
			# Retrieve Data
			ret[tick].tempo = self.states[t].tempo
			ret[tick].timeSig = self.states[t].timeSig
			for n in self.states[t].notesOff:
				if n in ret[tick].notes:
					ret[tick].notes.pop(n)
			for (n, v) in self.states[t].notesOn:
				v = float(v) / 127.0
				ret[tick].notes[n] = v
		return Action.SerializeAll(ret)
	@staticmethod
	def Deserialize(name: str, data: np.ndarray, ticksPerBeat: int):
		data = Action.DeserializeAll(data)
		time: int = 0
		lastNotes: List[int] = []
		lastState: MidiState = None
		ret = MidiTrack(name, ticksPerBeat)
		def IsAnythingNew(act: Action, last: MidiState) -> bool:
			nonlocal lastNotes
			if last.tempo != act.tempo or last.timeSig != act.timeSig or len(act.notes) != len(lastNotes):
				return True
			for (n, v) in last.notesOn:
				if not n in act.notes or round(act.notes[n] * 127) != v:
					return True
			for n in act.notes:
				if not n in lastNotes:
					return True
			return False
		for tick in range(0, len(data)):
			action: Action = data[tick]
			# Set Fields
			if lastState == None or IsAnythingNew(action, lastState):
				t = round(time)
				newState = MidiState(t)
				newState.tempo = action.tempo
				newState.timeSig = action.timeSig
				for n in action.notes:
					v = round(action.notes[n] * 127)
					if not n in lastNotes:
						newState.notesOn.append((n, v))
				for n in lastNotes:
					if not n in action.notes:
						newState.notesOff.append(n)
				ret.states[t] = lastState = newState
				lastNotes = action.notes
			# Update Time
			timeStep = ticksPerBeat / ((action.tempo / 4) / (Action.RESOLUTION * 60))
			time += timeStep
		ret.times = sorted(list(ret.states.keys()))
		return ret

class MidiObject:
	name: str
	tracks: Dict[str, MidiTrack]
	@property
	def TracksList(self) -> List[MidiTrack]:
		return list(self.tracks.values())
	def GetTrackByProgram(self, program: str = '000') -> Union[MidiTrack, None]:
		for t in self.tracks:
			if t.startswith(program):
				return self.tracks[t]
		return None
	def __init__(self, name: str, ticksPerBeat: int):
		self.name = name
		self.tpb = ticksPerBeat
		self.tracks: Dict[str, MidiTrack] = {}
	# Saving
	def SaveToFile(self, path: str) -> None:
		# print("Saving {} to file '{}' using TPB = {}".format(self.name, path, self.tpb))
		def SaveTrackToFile(track: MidiTrack, tStream: MidoMidiTrack) -> None:
				dTime: int = 0
				lastState: MidiState = MidiState(0)
				lastState.time = 0
				lastState.tempo = None
				lastState.timeSig = None
				npts = track.name.split(':')
				for t in track.times:
					nextState: MidiState = track.states[t]
					dTime = nextState.time - lastState.time
					if nextState.tempo != lastState.tempo:
						msg = MetaMessage('set_tempo', time=dTime, tempo=BpmToTempo(nextState.tempo))
						tStream.append(msg)
						dTime = 0
					if nextState.timeSig != lastState.timeSig:
						msg = MetaMessage('time_signature', time=dTime, numerator=nextState.timeSig[0], denominator=nextState.timeSig[1])
						tStream.append(msg)
						dTime = 0
					for n in nextState.notesOn:
						msg = Message('note_on', time=dTime, note=n[0], velocity=n[1])
						tStream.append(msg)
						dTime = 0
					for n in nextState.notesOff:
						msg = Message('note_off', time=dTime, note=n, velocity=0)
						tStream.append(msg)
						dTime = 0
					if dTime == 0:
						lastState = nextState
				tStream.append(MetaMessage('end_of_track', time=dTime))
		with MidiFile(None, None, 2, round(self.tpb / 2), 'utf-8', True) as stream:
			for tId in self.tracks.keys():
				(tPrg, tName) = tId.split(':')
				trackStream = stream.add_track(tName)
				trackStream.append(Message('program_change', time=0, program=int(tPrg)))
				SaveTrackToFile(self.tracks[tId], trackStream)
			stream.play()
			stream.save(path)
	# Parsing
	def ParseFromStreamType0(self, stream: MidiFile, clearTracks: bool = True):
		# Type 0 - which combines all the tracks or staves in to a single track.
		if clearTracks:
			self.tracks: Dict[str, MidiTrack] = {}
		# In Type 0 All tracks are merged; So we need to separate them from Channels
		# print('{} : Parsing from Stream, Type 0'.format(self.name))
		raise NotImplementedError()
	def ParseFromStreamType1(self, stream: MidiFile, clearTracks: bool = True):
		# Type 1 - saves the files as separate tracks or staves for a complete score with the tempo and time signature information only included in the first track.
		if clearTracks:
			self.tracks: Dict[str, MidiTrack] = {}
		# So we need to copy the Tempo and Time Signature to other tracks
		# print('{} : Parsing from Stream, Type 1'.format(self.name))
		boundChannels: list = ['000:Default'] * 16
		targetTrack = boundChannels[0]
		timeTrack = MidiTrack('xxx:Time', self.tpb)
		time: int = 0
		def GetTrack(channel: int) -> MidiTrack:
			trackId = targetTrack if channel < 0 else boundChannels[channel]
			if not trackId in self.tracks:
				# This is what we need to do because of Midi stream being Type 1
				# We need to copy all the Tempo and Time Signature data to newly created tracks
				newTrack = timeTrack.Copy(trackId)
				self.tracks[trackId] = newTrack
			# print("Targetting Track '{}'".format(trackId))
			return self.tracks[trackId]
		def ParseTrackName(m: Message) -> None:
			nonlocal targetTrack
			targetTrack = '{}:{}'.format(targetTrack.split(':')[0], m.name)
		def ParseProgram(m: Message) -> None:
			nonlocal targetTrack
			targetTrack = '{:03d}:{}'.format(m.program, targetTrack.split(':')[1])
			boundChannels[m.channel] = targetTrack
		def ParseTempo(m: Message) -> None:
			# Type 1, Do it on Time track, Later to be Merged
			timeTrack.GetOrCreateStateAt(time).SetTempo(TempoToBpm(m.tempo))
			for tId in list(self.tracks.keys()):
				self.tracks[tId].GetOrCreateStateAt(time).SetTempo(TempoToBpm(m.tempo))
		def ParseTimeSig(m: Message) -> None:
			# Type 1, Do it on Time track, Later to be Merged
			timeTrack.GetOrCreateStateAt(time).SetTimeSig(m.numerator, m.denominator)
			for tId in list(self.tracks.keys()):
				self.tracks[tId].GetOrCreateStateAt(time).SetTimeSig(m.numerator, m.denominator)
		def ParseNoteOff(m: Message) -> None:
			GetTrack(m.channel).GetOrCreateStateAt(time).ReleaseNote(m.note)
		def ParseNoteOn(m: Message) -> None:
			if m.velocity > 0:
				GetTrack(m.channel).GetOrCreateStateAt(time).PressNote(m.note, m.velocity)
			else:
				ParseNoteOff(m)
		def DoNothing(m: Message) -> None:
			pass
		Parse = {
			'track_name': ParseTrackName,
			'program_change': ParseProgram,
			'set_tempo': ParseTempo,
			'time_signature': ParseTimeSig,
			'key_signature': DoNothing,
			'control_change': DoNothing,
			'note_on': ParseNoteOn,
			'note_off': ParseNoteOff,
			'pitchwheel': DoNothing,
			'end_of_track': DoNothing,
			'text': DoNothing,
			'copyright': DoNothing,
			'channel_prefix': DoNothing,
			'midi_port': DoNothing
		}
		for message in stream:
			dTime = int(message.time * stream.ticks_per_beat * 2)
			time += dTime
			if message.type in Parse:
				Parse[message.type](message)
			else:
				raise Exception("Midi Message type '{}' not supported".format(message.type))
	def ParseFromStreamType2(self, stream: MidiFile, clearTracks: bool = True):
		# Type 2 - saves the file as separate tracks or staves and also includes the tempo and time signatures for each track.
		if clearTracks:
			self.tracks: Dict[str, MidiTrack] = {}
		# Can't be simpler than that !
		# print('{} : Parsing from Stream, Type 2'.format(self.name))
		boundChannels: list = ['000:Default'] * 16
		targetTrack = boundChannels[0]
		time: int = 0
		def GetTrack(channel: int) -> MidiTrack:
			trackId = targetTrack if channel < 0 else boundChannels[channel]
			if not trackId in self.tracks:
				newTrack = MidiTrack(trackId, self.tpb)
				self.tracks[trackId] = newTrack
			# print("Targetting Track '{}'".format(trackId))
			return self.tracks[trackId]
		def ParseTrackName(m: Message) -> None:
			nonlocal targetTrack
			targetTrack = '{}:{}'.format(targetTrack.split(':')[0], m.name)
		def ParseProgram(m: Message) -> None:
			nonlocal targetTrack
			targetTrack = '{:03d}:{}'.format(m.program, targetTrack.split(':')[1])
			boundChannels[m.channel] = targetTrack
		def ParseTempo(m: Message) -> None:
			# Type 2, Do it only on current track
			GetTrack(-1).GetOrCreateStateAt(time).SetTempo(TempoToBpm(m.tempo))
		def ParseTimeSig(m: Message) -> None:
			# Type 2, Do it only on current track
			GetTrack(-1).GetOrCreateStateAt(time).SetTimeSig(m.numerator, m.denominator)
		def ParseNoteOff(m: Message) -> None:
			GetTrack(m.channel).GetOrCreateStateAt(time).ReleaseNote(m.note)
		def ParseNoteOn(m: Message) -> None:
			if m.velocity > 0:
				GetTrack(m.channel).GetOrCreateStateAt(time).PressNote(m.note, m.velocity)
			else:
				ParseNoteOff(m)
		def DoNothing(m: Message) -> None:
			pass
		Parse = {
			'track_name': ParseTrackName,
			'program_change': ParseProgram,
			'set_tempo': ParseTempo,
			'time_signature': ParseTimeSig,
			'key_signature': DoNothing,
			'control_change': DoNothing,
			'note_on': ParseNoteOn,
			'note_off': ParseNoteOff,
			'pitchwheel': DoNothing,
			'end_of_track': DoNothing,
			'text': DoNothing,
			'copyright': DoNothing,
			'channel_prefix': DoNothing,
			'midi_port': DoNothing
		}
		for track in stream.tracks:
			time = 0
			for message in track:
				time += message.time
				if message.type in Parse:
					Parse[message.type](message)
				else:
					raise Exception("Midi Message type '{}' not supported".format(message.type))
	# Loading
	@staticmethod
	def LoadFromFile(path: str):
		from os.path import basename, splitext
		stream = MidiFile(path)
		ret = MidiObject(basename(splitext(path)[0]), stream.ticks_per_beat * 2)
		if stream.type == 0:
			ret.ParseFromStreamType0(stream)
		elif stream.type == 1:
			ret.ParseFromStreamType1(stream)
		elif stream.type == 2:
			ret.ParseFromStreamType2(stream)
		else:
			raise Exception('Invalid MIDI type. Expected 0, 1 or 2; Found {}'.format(stream.type))
		return ret
