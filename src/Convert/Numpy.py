import numpy as np
from PIL import Image
from io import BytesIO
from os.path import basename
from typing import List, Tuple
from Data.Midi import Action

def AppendHorizontal(vals: tuple):
	return np.hstack(vals)

def SplitHorizontal(ret):
	hLen = ret.shape[1]
	if (hLen % Action.NEURON_COUNT) != 0:
		raise Exception('Invalid Length : {0} % {1} = {2}'.format(hLen, Action.NEURON_COUNT, hLen % Action.NEURON_COUNT))
	hCount = int(hLen / Action.NEURON_COUNT)
	splits = []
	if len(splits) == 1:
		return ret
	for i in range(hCount):
		splits.append(hLen)
	return np.hsplit(ret, hCount)

def SerializeInstrument(actions: List[Action]):
	aCount = len(actions)
	ret = np.zeros(
		(aCount, Action.NEURON_COUNT),
		np.bool,
	)
	# print('Serializing ({:04d}) Actions'.format(aCount))
	for i in range(aCount):
		a: Action = actions[i]
		# print('Serialized ({:04d}) {}'.format(i, a))
		ret[i] = a.SerializeToBools()
	return ret

def DeserializeInstrument(arr):
	ret = []
	for i in range(arr.shape[0]):
		a: Action = Action.DeserializeFromBools(arr[i])
		# print('Deserialized ({:04d}) {}'.format(i, a))
		ret.append(a)
	return ret


def ActionsToImage(acts: Tuple[str, List[Action]], path: str):
	name = acts[0]
	acts = acts[1]
	# print(f'Serializing {len(acts)} Actions for Png {path}')
	acts = SerializeInstrument(acts)
	# print('Saving Png file {}'.format(path))
	NumpyToImage(acts, path)

def NumpyToImage(arr, path: str) -> np.array:
	arr = arr.astype('float')
	arrMin = np.min(arr)
	arrMax = np.max(arr)
	arr = (arr - arrMin) / (arrMax - arrMin)
	img: Image = Image.fromarray(arr * 255).convert('L')
	img.save(path)

def ImageToNumpy(path: str) -> np.array:
	# print(F"Loading Png file {path}")
	img: Image = Image.open(path)
	arr = np.asarray(img).astype(float)
	arrMin = np.min(arr)
	arrMax = np.max(arr)
	arr = (arr - arrMin) / (arrMax - arrMin)
	return arr

def ImageToActions(name: str, path: str) -> Tuple[str, List[Action]]:
	# print(F"Deserializing Actions from '{path}'")
	return (
		name,
		DeserializeInstrument(ImageToNumpy(path))
	)